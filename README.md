# music-app

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Docker

````
Make sure you have docker installed

build local-env: docker build -t devops-project .

run :  docker run -it -v ${PWD}:/usr/src/app -v /usr/src/app/node_modules -p 5000:5000 devops-project


build local-prod: docker build -f Dockerfile-prod -t devops-project-prod .
run :  docker run -it -p 80:80 --rm devops-project-prod
````
### Access project

````
local-env:  http://localhost:5000/


local-prod: http://localhost/
````



